# -*- coding: utf-8 -*-
"""
Created on Wed Dec 14 07:22:17 2016

@author: ik-be
"""

# Copyright (C) 2013 by Aivars Kalvans <aivars.kalvans@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import re
import random
import base64
import logging
import requests
from bs4 import BeautifulSoup
from collections import defaultdict
from stem import Signal, CircStatus, SocketError
from stem.control import Controller

log = logging.getLogger('scrapy.proxies')



class RandomProxy(object):
    def __init__(self, settings):
        self.proxy_list = settings.get('PROXY_LIST')
        self.chosen_proxy = ''
        self.IP_per_port = {}
        self.new_id_refresh_count = defaultdict(int)
        self.CONTROL_port_nr_per_ip = {}
        self.port_new_id_thresh = {}
        self.base_socks_port = 9050
        self.base_control_port = 8118 #Must align with whats in tor_polipo_config.py
        self.refresh_interval_tuple = (70, 130)
        self.previous_batch_IP_per_port = {}
        
        if self.proxy_list is None:
            raise KeyError('PROXY_LIST setting is missing')
        fin = open(self.proxy_list)
        self.proxies = {}
        for i, line in enumerate(fin.readlines()): #Populate proxy dict
            parts = re.match('(\w+://)(\w+:\w+@)?(.+)', line.strip())
            if not parts:
                continue

            # Cut trailing @
            if parts.group(2):
                user_pass = parts.group(2)[:-1]
            else:
                user_pass = ''

            proxy_address = parts.group(1) + parts.group(3)
            self.port_new_id_thresh[proxy_address] = random.randint(*self.refresh_interval_tuple) 
            self.CONTROL_port_nr_per_ip[proxy_address] = self.base_control_port + i
            self.proxies[proxy_address] = user_pass
            
        fin.close() 
        self.__populate_IP_per_port_dict()
        print "PROXY IP DICT: {}".format(self.IP_per_port)

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings)

    def process_request(self, request, spider):
        # Don't overwrite with a random one (server-side state for IP)
        if 'proxy' in request.meta:
            if request.meta["exception"] is False:
                return
        request.meta["exception"] = False
        if len(self.proxies) == 0:
            raise ValueError('All proxies are unusable, cannot proceed')

        proxy_address = random.choice(list(self.proxies.keys()))
        proxy_user_pass = self.proxies[proxy_address]
        self.new_id_refresh_count[proxy_address] += 1
        
        #control_port_nr = self.CONTROL_port_nr_per_ip[proxy_address]

        if self.new_id_refresh_count[proxy_address] >= self.port_new_id_thresh[proxy_address]:
            self.__rotate_ip(proxy_address) 
    
        if proxy_user_pass:
            request.meta['proxy'] = proxy_address
            basic_auth = 'Basic ' + base64.b64encode(proxy_user_pass.encode()).decode()
            request.headers['Proxy-Authorization'] = basic_auth
        else:
            request.meta['proxy'] = proxy_address                       #Added myself.####################################
            log.debug('Proxy user pass not found')
        log.debug('Using proxy <%s>, %d proxies left' % (
                proxy_address, len(self.proxies)))

    def process_exception(self, request, exception, spider):
        if 'proxy' not in request.meta:
            return
        proxy_address = request.meta['proxy']
        try:
            del self.proxies[proxy_address] #dont remove but instead get new ID?
        except KeyError:
            pass
        request.meta["exception"] = True
        log.info('Removing failed proxy <%s>, %d proxies left' % (
            proxy_address, len(self.proxies)))




    def __new_useragent(self, port_nr):
        self.headers[str(port_nr)]['User-Agent'] = self.UA.random #Get new UA from pregenerated list of names             

    def __get_new_id(self, control_port_nr, proxy_address):
        self.__newI(control_port_nr)
        #self.__new_useragent(port_nr)
        self.new_id_refresh_count[proxy_address] = 0 #Reset the batch counter
        self.port_new_id_thresh[proxy_address] = random.randint(*self.refresh_interval_tuple)

    def __get_port_nr(self, localhost_port_string):
        return int("".join([s for s in localhost_port_string if s.isdigit()]))            
                                                 
    def __newI(self, control_port_nr, rotate_user_agent = False):
        #Request new IP for a control_port number (8118++)
        with Controller.from_port(port = control_port_nr) as controller:
            controller.authenticate("poep") #HashedControlPassword: 16:1386B1D025289B59604B0139231A2A9490A75D87F8E63FAAFA9ABE2EBB see: https://stem.torproject.org/tutorials/the_little_relay_that_could.html
            controller.signal(Signal.NEWNYM) 

    def __rotate_ip(self, proxy_address):
        control_port_nr = self.CONTROL_port_nr_per_ip[proxy_address]
        old_ip = self.__get_ip_adress(control_port_nr)
        self.__get_new_id(control_port_nr, proxy_address)
        new_ip = self.__get_ip_adress(control_port_nr)
        self.__check_ip(new_ip, old_ip, proxy_address)


    def __check_ip(self, new_ip, old_ip, proxy_address):
        if new_ip not in self.IP_per_port.values() and old_ip not in self.previous_batch_IP_per_port.values():
            self.IP_per_port[proxy_address] = new_ip
            self.previous_batch_IP_per_port[proxy_address] = old_ip
            
        else:
            self.__rotate_ip(proxy_address)


    def __populate_IP_per_port_dict(self):
        for proxy_address, control_port_nr in self.CONTROL_port_nr_per_ip.items():
            self.IP_per_port[proxy_address] = self.__get_ip_adress(proxy_address, control_port_nr)


    def __get_ip_adress(self, proxy_address, control_port_nr):
        try:
            with Controller.from_port(port = control_port_nr) as controller:
                controller.authenticate("poep")
                for circ in controller.get_circuits(): #First circ is the outgoing IP.
                    if circ.status != CircStatus.BUILT:
                        continue
        
                    exit_fp, exit_nickname = circ.path[-1]
                    exit_desc = controller.get_network_status(exit_fp, None)
                    exit_ip_address = exit_desc.address if exit_desc else 'unknown'
                    return exit_ip_address #Move outside for loop to build the entire tor circuit 
        except SocketError:
            print "Connection has been refused on proxy adress {} with control portnr {}".format(proxy_address, control_port_nr)
            #It's  silly to have so many tracking dicts.
            try:
                del self.IP_per_port[proxy_address]
                del self.CONTROL_port_nr_per_ip[proxy_address]
                del self.port_new_id_thresh[proxy_address]
                del self.proxies[proxy_address]
            except KeyError:
                print "KEY NOT FOUND: {}".format(proxy_address)
 




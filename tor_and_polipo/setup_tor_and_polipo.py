import os
from subprocess import call, Popen
import tor_polipo_config as tpc

class Setup_stuff(object):
    def build_folder(self, path):
        try:
            os.mkdir(path)
        except OSError:
            print "Directory {} is already made".format(path)        
 
    def write_file(self, path, _file):
        with open(path, 'w') as f:
            f.write(_file)
    
    def write_list_of_strings(self, path, string_list):
        thefile = open(path, 'w')
        for item in string_list:
            thefile.write("%s\n" % item)

    def call_file(self, absolute_path):
        if os.path.isfile(absolute_path):
            #Call returns a call code, Popen returns an object
            call_code = call(absolute_path, shell=True)
            return call_code
        else:
            return "File not found on: {}".format(self.tor_bash_script_file_path)
            
    def give_file_permission(self, absolute_path):
        return call(['chmod', '+x', absolute_path])        

                

class Setup_tor(object):

    def __init__(self):
        self.base_or_port_range =range(tpc.BASE_PORT, tpc.BASE_PORT + tpc.DAEMON_AMOUNT) 
        self.base_control_port_range = range(tpc.BASE_CONTROL_PORT, tpc.BASE_CONTROL_PORT + tpc.DAEMON_AMOUNT)
        self.pid_dict = {} #What tors are running at the moment
        self.socks_str = 'SocksPort'
        self.control_str = 'ControlPort'
        self.h = Setup_stuff()
        
        
    #TODO: add PID - Port handler, if a tor process dies on a PID - kill it or make sure it's dead and restart a new tor process
    #TODO: make seperate bash script for single port calling (rewrite it from python?)
    #Keep list of open ports in the 9050+ range
    
    def build_tor_folder(self):
       self.build_folder(os.path.join(tpc.MAIN_PATH, 'Tor_folder'))
        
       
    def give_tor_file_permission(self):
        "If permission is denied, use the following command on the file: >chmod +x start_tor.sh"
        return self.h.give_file_permission(tpc.TOR_BASH_SCRIPT_FILE_NAME)
         
           
    def write_bash_script_file(self, amount = None):
        if amount is None:
            amount = tpc.DAEMON_AMOUNT
        self.tor_script = tpc.TOR_BASH_SCRIPT % (tpc.BASE_PORT, tpc.BASE_CONTROL_PORT, (amount - 1))
        self.h.write_file(tpc.TOR_BASH_SCRIPT_FILE_NAME, self.tor_script) #Bash for loop logic
        return "Written file succesfully."
        
    def start_tor_daemons(self):
        return self.h.call_file(tpc.TOR_BASH_SCRIPT_FILE_NAME)
        
    def check_if_all_running(self):
        self.get_tor_pids()
        counter = 0
        pid_control_nrs = [i[0] for i in self.pid_dict.values()]
        for control_port_nr in self.base_control_port_range:
            if control_port_nr in pid_control_nrs:
                counter += 1
        return counter >= tpc.DAEMON_AMOUNT 
        
    def run_tor(self):
        if self.check_if_all_running():
            return "There are more or equal amount of tor daemons running at the moment on the following pids: {}".format(str(self.pid_dict.keys()))
        else:
            self.start_tor_daemons()
            if self.check_if_all_running():
                print "Some tor instances have failed to start. Retrying." #Happens sometimes
                self.run_tor()
            return "Tor daemons started"
        
    def kill_tor(self, pid_nr):
        call(['kill', str(pid_nr)])
        del(self.pid_dict[pid_nr])
        
    def kill_all_tor(self):
        for pid in self.pid_dict:
            self.kill_tor(pid)
            
    def get_port_nr(self, word, string):
        control_index = string.index(word) + len(word) + 1
        return int(string[control_index:control_index + 4])
                
        
    def get_tor_pids(self):
        pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]
        cmd_string = '--HashedControlPassword'
        for pid in pids:
            try:
                pid_cmdline_name = open(os.path.join('/proc', pid, 'cmdline'), 'rb').read()
                if cmd_string in pid_cmdline_name:
                    control_nr = self.get_port_nr(self.control_str, pid_cmdline_name)
                    socks_nr = self.get_port_nr(self.socks_str, pid_cmdline_name)
                    self.pid_dict[pid] = (control_nr, socks_nr)
            except IOError: # process has already terminated
                print "PID: {} has already been terminated".format(pid)
                continue
                 
    def get_temp_pid_list(self):
        return self.pid_dict.keys()



class Setup_polipo(object):
    def __init__(self):
        self.h = Setup_stuff()
        
    def setup_polipo_nr(self, NUMBER = 0):
        polipo_log_file_path = os.path.join(tpc.POLIPO_PATH, 'polipo{}.log'.format(NUMBER))
        polipo_proxy_port_nr = tpc.POLIPO_PROXY_PORT + NUMBER
        polipo_socks_parent_proxy_nr = tpc.BASE_PORT + NUMBER
        polipo_chache = tpc.POLIPO_CACHE.format(NUMBER) #Make this directory 
        poliporc_file_name = tpc.POLIPORC_FILENAME.format(NUMBER)
        #polipo_http_proxy_scrapy_adress = 'localhost:{}'.format(polipo_proxy_port_nr)
        print polipo_log_file_path, polipo_proxy_port_nr, polipo_socks_parent_proxy_nr, polipo_chache
        polipo_config_file_content = tpc.POLIPO_CONFIG_FILE_CONTENT.format(polipo_log_file_path, polipo_proxy_port_nr, polipo_socks_parent_proxy_nr, polipo_chache)

        self.h.write_file(poliporc_file_name, polipo_config_file_content)
        self.h.give_file_permission(poliporc_file_name)
        self.h.build_folder(polipo_chache)
        return True
        
    def run_polipo(self, polipo_nr):
        if self.setup_polipo_nr(polipo_nr):
            Popen([ 'polipo', '-c', tpc.POLIPORC_FILENAME.format(polipo_nr)])
        else:
            print "Something went wrong with the setup of polipo nr: {}".format(polipo_nr)        
            
    def start_all_daemon_polipo(self):
        proxy_file_list = []
        for i in range(tpc.DAEMON_AMOUNT):
            polipo_proxy_port_nr = tpc.POLIPO_PROXY_PORT + i
            proxy_file_list.append('http://localhost:{}'.format(polipo_proxy_port_nr))
            self.run_polipo(i)   
        
        self.h.write_list_of_strings(tpc.PROXY_FILE_LOCATION, proxy_file_list)

        
        



TOR_SETUP = Setup_tor()  
TOR_SETUP.write_bash_script_file()
TOR_SETUP.give_tor_file_permission()
TOR_SETUP.run_tor()

"""
print torpids
for pid in torpids:
    RD.TOR_SETUP.kill_tor(pid)

print RD.TOR_SETUP.pid_dict
#"""

Setup_polipo().start_all_daemon_polipo()
















